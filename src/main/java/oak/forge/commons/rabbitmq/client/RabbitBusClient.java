package oak.forge.commons.rabbitmq.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import lombok.*;
import oak.forge.commons.bus.*;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.data.message.*;
import oak.forge.commons.rabbitmq.exception.RabbitClientConnectionException;
import oak.forge.commons.rabbitmq.topic.TopicFactory;
import oak.forge.commons.rabbitmq.connection.*;
import org.slf4j.*;

import java.util.*;

import static oak.forge.commons.rabbitmq.topic.MessageRoutingType.*;

public class RabbitBusClient extends MessageBus {

    Logger logger = LoggerFactory.getLogger(RabbitBusClient.class);

    private final ObjectMapper objectMapper;
    private final TopicFactory topicFactory;

    Channel channel;
    private final UUID clientId;

    public RabbitBusClient(RabbitConnectionFactory rabbitConnectionFactory, TopicFactory topicFactory, ObjectMapper objectMapper) throws RabbitClientConnectionException {
        this.objectMapper = objectMapper;
        this.topicFactory = topicFactory;
        clientId = UUID.randomUUID();
        channel = rabbitConnectionFactory.initializeConfiguredConnection(topicFactory,defaultConsumer, clientId);
    }

    @SneakyThrows
    @Override
    public <M extends Message> void post(M m) {
        RabbitMqMessage rabbitMqMessage = prepareRabbitMqMessage(m);
        String exchange = topicFactory.getPublishExchange(rabbitMqMessage.getRoutingKey());
        String routingKey = rabbitMqMessage.getRoutingKey();
        byte[] byteMessage = objectMapper.writeValueAsBytes(rabbitMqMessage);
        logger.info("Sending from :" + topicFactory.getConfigName() + " " + routingKey);
        channel.basicPublish(exchange, routingKey, null, byteMessage);
    }

    private <M extends Message> RabbitMqMessage prepareRabbitMqMessage(M message) throws JsonProcessingException {
        topicFactory.handleSender(message, clientId);
        String messageClass = message.getClass().getName();
        String payload = objectMapper.writeValueAsString(message);
        String messageRouting = getMessageRouting(message);
        String routingKey = getRoutingKey(message, messageRouting);
        return new RabbitMqMessage(routingKey, messageClass, payload);
    }

    String getMessageRouting(Message message) {
        String routing;
        if (message instanceof Command) {
            routing = COMMAND.getRouting();
        } else if (message instanceof DomainError) {
            routing = DOMAIN_ERROR.getRouting();
        } else if (message instanceof GenericError) {
            routing = GENERIC_ERROR.getRouting();
        } else if (message instanceof Event) {
            routing = EVENT.getRouting();
        } else if (message instanceof Query) {
            routing = QUERY.getRouting();
        } else if (message instanceof QueryResponse) {
            routing = QUERY_RESPONSE.getRouting();
        } else {
            routing = NO_TYPE.getRouting();
        }
        return routing;
    }

    String getRoutingKey(Message message, String messageType) {
        return message.getReceiverId() != null ? messageType + message.getReceiverId() : messageType + "public";
    }

    private final DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
        @SneakyThrows
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            long deliveryTag = envelope.getDeliveryTag();
            logger.info("Consuming as " + topicFactory.getConfigName() + " with ID " + clientId);
            RabbitMqMessage rabbitMqMessage = objectMapper.readValue(body, RabbitMqMessage.class);
            Class<?> messageClass = Class.forName(rabbitMqMessage.getMessageType());
            Object object = objectMapper.readValue(rabbitMqMessage.getPayload(), messageClass);
            Message message = (Message) object;
            handle(message);
            channel.basicAck(deliveryTag, false);
        }
    };


    // TODO: try to avoid this method
    @Override
    public <E extends Event> void subscribe(Class<E> aClass, MessageHandler<E> messageHandler) {

    }
}

