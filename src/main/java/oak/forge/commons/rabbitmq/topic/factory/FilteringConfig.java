package oak.forge.commons.rabbitmq.topic.factory;

import com.rabbitmq.client.Channel;
import oak.forge.commons.data.message.Message;
import oak.forge.commons.rabbitmq.topic.*;

import java.io.IOException;
import java.util.*;

import static oak.forge.commons.rabbitmq.topic.MessageRoutingType.COMMAND;
import static oak.forge.commons.rabbitmq.topic.MessageRoutingType.QUERY;

public class FilteringConfig extends TopicFactory {

    private final static String DEFAULT_PUBLISH_EXCHANGE = TopicData.SECURED_INCOMING_EXCHANGE;
    private final static String ERROR_PUBLISH_EXCHANGE = TopicData.SECURED_OUTGOING_EXCHANGE;

    public FilteringConfig() {
        super("filtering");
    }

    @Override
    public void declareQueuesConfig(Channel channel, UUID clientId) throws IOException {
        channel.exchangeDeclare(DEFAULT_PUBLISH_EXCHANGE, TopicData.TYPE_TOPIC);
        channel.exchangeDeclare(TopicData.UNSECURED_EXCHANGE, TopicData.TYPE_TOPIC);

        queueDeclare(channel, TopicData.UNSECURED_QUEUE);

        queueBind(channel, TopicData.UNSECURED_QUEUE, TopicData.UNSECURED_EXCHANGE, "*.*");
    }

    @Override
    public String getPublishExchange(String routingKey) {
        if (routingKey.startsWith(COMMAND.getRouting()) || routingKey.startsWith(QUERY.getRouting())) {
            return DEFAULT_PUBLISH_EXCHANGE;
        }
        return ERROR_PUBLISH_EXCHANGE;
    }

    @Override
    public List<String> getQueuesToSubscribe(UUID clientId) {
        List<String> queuesToSubscribe = new ArrayList<>();
        queuesToSubscribe.add(TopicData.UNSECURED_QUEUE);
        return queuesToSubscribe;
    }


    @Override
    public <M extends Message> void handleSender(M message, UUID clientId) {
    }

}
