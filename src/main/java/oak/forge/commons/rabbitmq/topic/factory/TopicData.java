package oak.forge.commons.rabbitmq.topic.factory;

public class TopicData {
    public final static String TYPE_TOPIC = "topic";
    public final static String TYPE_DIRECT = "direct";
    public final static String TYPE_FANOUT = "fanout";

    public final static String UNSECURED_EXCHANGE = "unsecured";
    public final static String SECURED_INCOMING_EXCHANGE = "secured.in";
    public final static String SECURED_OUTGOING_EXCHANGE = "secured.out";

    public final static String UNSECURED_QUEUE = "message-unsecured";
    public final static String SECURED_QUEUE = "message-secured";
    public final static String EVENTS_QUEUE = "events-secured";

    /*
    it is only first part of private routing key;
    to make private routing key completed,
    it is necessary to add client or object ID in the end of
    routing key
     */
    public final static String PRIVATE_ROUTING_KEY = "*.";
    public final static String PUBLIC_ROUTING_KEY = "*.public";
    public final static String EVENTS_ROUTING_KEY = "event.*";

    private TopicData(){}
}
