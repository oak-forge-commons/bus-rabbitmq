package oak.forge.commons.rabbitmq.topic.factory;

import com.rabbitmq.client.Channel;
import oak.forge.commons.data.message.Message;
import oak.forge.commons.rabbitmq.topic.*;

import java.io.IOException;
import java.util.*;

public class AggregateConfig extends TopicFactory {

    private final static String DEFAULT_PUBLISH_EXCHANGE = TopicData.SECURED_OUTGOING_EXCHANGE;

    public AggregateConfig() {
        super("aggregate");
    }

    @Override
    public void declareQueuesConfig(Channel channel, UUID clientId) throws IOException {
        String exclusiveQueue = getExclusiveQueue(getConfigName(), clientId);
        String privateRoutingKey = getPrivateRoutingKey(clientId);

        channel.exchangeDeclare(DEFAULT_PUBLISH_EXCHANGE, TopicData.TYPE_TOPIC);
        channel.exchangeDeclare(TopicData.SECURED_INCOMING_EXCHANGE, TopicData.TYPE_TOPIC);

        queueDeclare(channel, TopicData.SECURED_QUEUE);
        queueDeclareExclusive(channel, exclusiveQueue);

        queueBind(channel, exclusiveQueue, TopicData.SECURED_OUTGOING_EXCHANGE, privateRoutingKey);
        queueBind(channel, TopicData.SECURED_QUEUE, TopicData.SECURED_INCOMING_EXCHANGE, "*.*");
    }

    @Override
    public String getPublishExchange(String routingKey) {
        return DEFAULT_PUBLISH_EXCHANGE;
    }

    @Override
    public List<String> getQueuesToSubscribe(UUID clientId) {
        List<String> queuesToSubscribe = new ArrayList<>();
        queuesToSubscribe.add(getExclusiveQueue(getConfigName(), clientId));
        queuesToSubscribe.add(TopicData.SECURED_QUEUE);
        return queuesToSubscribe;
    }


    @Override
    public <M extends Message> void handleSender(M message, UUID clientId) {
    }

}
