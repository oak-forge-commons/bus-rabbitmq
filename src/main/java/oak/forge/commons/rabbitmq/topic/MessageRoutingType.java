package oak.forge.commons.rabbitmq.topic;

import lombok.Getter;

@Getter
public enum MessageRoutingType {
    
    COMMAND("command."),
    EVENT("event."),
    QUERY("query."),
    QUERY_RESPONSE("queryResponse."),
    DOMAIN_ERROR("domainError."),
    GENERIC_ERROR("genericError."),
    NO_TYPE("-.");

    private final String routing;

    MessageRoutingType(String routing) {
        this.routing = routing;
    }

}
