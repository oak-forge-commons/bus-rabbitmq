package oak.forge.commons.rabbitmq.topic;

import com.rabbitmq.client.Channel;
import lombok.Getter;
import oak.forge.commons.data.message.Message;
import oak.forge.commons.rabbitmq.topic.factory.TopicData;

import java.io.IOException;
import java.util.*;

@Getter
public abstract class TopicFactory {

    private final String configName;

    protected TopicFactory(String configName) {
        this.configName = configName;
    }

    /*
    declare exchange, queues for different strategies
    */
    public abstract void declareQueuesConfig(Channel channel, UUID clientId) throws IOException;

    /*
    return exchange for the used strategy,
    to which messages should be sent
     */
    public abstract String getPublishExchange(String routingKey);

    public abstract List<String> getQueuesToSubscribe(UUID clientId);

    public abstract <M extends Message> void handleSender(M message, UUID clientId);

    public String getExclusiveQueue(String strategyName, UUID clientId) {
        return strategyName + "-" + clientId;
    }

    public String getPrivateRoutingKey(UUID clientId) {
        return TopicData.PRIVATE_ROUTING_KEY + clientId;
    }

    public void queueDeclare(Channel channel, String queueName) throws IOException {
        channel.queueDeclare(queueName, false, false, true, null);
    }

    public void queueDeclareExclusive(Channel channel, String queueName) throws IOException {
        channel.queueDeclare(queueName, false, true, true, null);
    }

    public void queueBind(Channel channel, String queueName, String exchange, String routingKey) throws IOException {
        channel.queueBind(queueName, exchange, routingKey);
    }
}
