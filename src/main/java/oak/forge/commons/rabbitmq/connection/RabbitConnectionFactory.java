package oak.forge.commons.rabbitmq.connection;

import com.rabbitmq.client.*;
import oak.forge.commons.rabbitmq.exception.*;
import oak.forge.commons.rabbitmq.topic.TopicFactory;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

public class RabbitConnectionFactory {

    private final RabbitConfigFactory configFactory;

    public RabbitConnectionFactory(RabbitConfigFactory configFactory) {
        this.configFactory = configFactory;
    }

    public Channel initializeConfiguredConnection(TopicFactory topicFactory, DefaultConsumer defaultConsumer, UUID clientId) throws RabbitClientConnectionException {
        try {
            ConnectionFactory factory = configFactory.getConnectionFactory();
            Connection connection = factory.newConnection(topicFactory.getConfigName() + clientId);
            Channel channel = connection.createChannel();
            topicFactory.declareQueuesConfig(channel, clientId);
            declareSubscription(topicFactory, channel, defaultConsumer, clientId);
            return channel;
        } catch (IOException | TimeoutException e) {
            throw new RabbitClientConnectionException("Create connection occur error. " + e);
        }
    }

    void declareSubscription(TopicFactory topicFactory, Channel channel, DefaultConsumer defaultConsumer, UUID clientId) {
        topicFactory.getQueuesToSubscribe(clientId).forEach(queue -> {
            try {
                channel.basicConsume(queue, false, "", defaultConsumer);
            } catch (IOException e) {
                throw new RabbitClientConsumerException("Error when consuming from queue: " + queue);
            }
        });
    }
}
