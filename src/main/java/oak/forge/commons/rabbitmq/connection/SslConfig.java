package oak.forge.commons.rabbitmq.connection;

import com.rabbitmq.client.SslContextFactory;
import lombok.*;
import org.apache.http.ssl.*;
import org.springframework.core.io.ClassPathResource;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.util.Objects;

public class  SslConfig implements SslContextFactory {

    private final String password;
    private final String keyStoreFile;
    private final String trustStoreFile;

    public SslConfig(String password, String keyStoreFile, String trustStoreFile) {
        this.password = password;
        this.keyStoreFile = keyStoreFile;
        this.trustStoreFile = trustStoreFile;
    }

    @SneakyThrows
    @Override
    public SSLContext create(String name) {
        final char[] password = this.password.toCharArray();
        final ClassPathResource keyStoreFile = new ClassPathResource(this.keyStoreFile);
        final ClassPathResource trustStoreFile = new ClassPathResource(this.trustStoreFile);

       SSLContext sslContext = SSLContexts.custom()
                .loadKeyMaterial(keyStoreFile.getURL(), password, password)
                .loadTrustMaterial(trustStoreFile.getURL(), password).build();

       return sslContext;
    }
}
