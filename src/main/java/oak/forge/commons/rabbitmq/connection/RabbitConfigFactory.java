package oak.forge.commons.rabbitmq.connection;

import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.*;

public class RabbitConfigFactory {

    Logger logger = LoggerFactory.getLogger(RabbitConfigFactory.class);

    private final RabbitConnectionData connectionConfig;
    private final SslConfig sslConfig;

    public RabbitConfigFactory(RabbitConnectionData connectionConfig, SslConfig sslConfig) {
        this.connectionConfig = connectionConfig;
        this.sslConfig = sslConfig;
    }

    public ConnectionFactory getConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(connectionConfig.getUsername());
        factory.setPassword(connectionConfig.getPassword());
        factory.setVirtualHost("/");
        factory.setHost(connectionConfig.getHost());
        factory.setPort(connectionConfig.getPort());
        if (connectionConfig.isSslOn()) {
            factory.setSslContextFactory(sslConfig);
        }
        return factory;
    }
}

