package oak.forge.commons.rabbitmq.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import oak.forge.commons.data.message.Command;


public class TestCommand extends Command {

    private String name;

    @JsonCreator
    public TestCommand(String name) {
        super();
        this.name = name;
    }

}

