package oak.forge.commons.rabbitmq.client

import com.rabbitmq.client.Channel
import oak.forge.commons.rabbitmq.utils.TestCommand
import oak.forge.commons.rabbitmq.utils.TestDomainError
import oak.forge.commons.rabbitmq.utils.TestEvent
import oak.forge.commons.rabbitmq.utils.TestGenericError
import oak.forge.commons.rabbitmq.utils.TestMessage
import oak.forge.commons.rabbitmq.connection.RabbitConnectionFactory
import oak.forge.commons.rabbitmq.mapper.DefaultObjectMapper
import oak.forge.commons.rabbitmq.topic.TopicFactory
import oak.forge.commons.rabbitmq.utils.TestQuery
import oak.forge.commons.rabbitmq.utils.TestQueryResponse
import spock.lang.Specification

import static oak.forge.commons.rabbitmq.topic.MessageRoutingType.*

class RabbitBusClientTest extends Specification {

    RabbitBusClient rabbitBusClient
    RabbitConnectionFactory connectionConfigFactory;
    TopicFactory topicFactory;
    def objectMapper = new DefaultObjectMapper()

    void setup() {
        connectionConfigFactory = Mock()
        topicFactory = Mock()
        rabbitBusClient = new RabbitBusClient(connectionConfigFactory, topicFactory, objectMapper)
    }

    def "should return part of routing key with correct instance"() {
        given:
        def message = messageToCheck

        when:
        def routing = rabbitBusClient.getMessageRouting(message)

        then:
        routing == expected

        where:
        messageToCheck                       || expected
        new TestCommand("command")           || COMMAND.getRouting()
        new TestEvent("event")               || EVENT.getRouting()
        new TestGenericError("genericError") || GENERIC_ERROR.getRouting()
        new TestMessage()                    || NO_TYPE.getRouting()
        new TestDomainError()                || DOMAIN_ERROR.getRouting()
        new TestQuery()                      || QUERY.getRouting()
        new TestQueryResponse()              || QUERY_RESPONSE.getRouting()
    }

    def "should return public routing key"() {
        given:
        def message = messageToCheck
        def messageType = messageRouting

        when:
        def routingKey = rabbitBusClient.getRoutingKey(message, messageType)

        then:
        routingKey == messageType + "public"

        where:
        messageToCheck                       || messageRouting
        new TestCommand("command")           || COMMAND.getRouting()
        new TestEvent("event")               || EVENT.getRouting()
        new TestGenericError("genericError") || GENERIC_ERROR.getRouting()
        new TestMessage()                    || NO_TYPE.getRouting()
        new TestDomainError()                || DOMAIN_ERROR.getRouting()
        new TestQuery()                      || QUERY.getRouting()
        new TestQueryResponse()              || QUERY_RESPONSE.getRouting()

    }

    def "should return private routing key"() {
        given:
        def message = messageToCheck
        def messageType = routing

        when:
        def routingKey = rabbitBusClient.getRoutingKey(message, messageType)

        then:
        routingKey == messageType + message.getReceiverId()

        where:
        messageToCheck                                   || routing
        new TestEvent(UUID.randomUUID(), "event")        || EVENT.getRouting()
        new TestGenericError(UUID.randomUUID(), "error") || GENERIC_ERROR.getRouting()
    }

    def "should invoke basic publish after posting message"() {
        given:
        TestCommand testCommand = new TestCommand("name")
        rabbitBusClient.channel = Mock(Channel)

        when:
        rabbitBusClient.post(testCommand)

        then:
        1 * rabbitBusClient.channel.basicPublish(_, _, _, _)
    }

    def "should publish message in rabbitmq message as string payload"() {
        given:
        TestCommand testCommand = new TestCommand("name")
        DefaultObjectMapper objectMapper = new DefaultObjectMapper()

        rabbitBusClient.channel = Mock(Channel)

        RabbitMqMessage rabbitMqMessage =
                new RabbitMqMessage("command.public", testCommand.getClass().getName(), objectMapper.writeValueAsString(testCommand))
        def bytes = objectMapper.writeValueAsBytes(rabbitMqMessage)

        when:
        rabbitBusClient.post(testCommand)

        then:
        1 * rabbitBusClient.channel.basicPublish(_, _, _, bytes)
    }

    def "should publish message with strategy exchange"() {
        given:
        TestCommand testCommand = new TestCommand("name1")
        rabbitBusClient.channel = Mock(Channel)

        when:
        rabbitBusClient.post(testCommand)

        then:
        1 * topicFactory.getPublishExchange(_) >> "exchange"
        1 * rabbitBusClient.channel.basicPublish("exchange", _, null, _)

    }

    def "should publish message with correct routing key"() {
        given:
        rabbitBusClient.channel = Mock(Channel)

        when:
        rabbitBusClient.post(messageToPost)

        then:
        1 * rabbitBusClient.channel.basicPublish(_, routingKey, null, _)

        where:
        messageToPost                              | routingKey
        new TestCommand("command")                 | "command.public"
        new TestEvent("event")                     | "event.public"
        new TestEvent(UUID.randomUUID(), "event2") | "event." + messageToPost.getReceiverId()

    }
}
