package oak.forge.commons.rabbitmq.topic

import com.rabbitmq.client.Channel
import oak.forge.commons.rabbitmq.topic.factory.AggregateConfig
import oak.forge.commons.rabbitmq.topic.factory.TopicData
import spock.lang.Shared

class AggregateConfigTest extends RabbitMqContainerConfig {

    @Shared
    AggregateConfig aggregateConfig

    @Shared
    Channel channel

    void setupSpec() {

        container.start()
        channel = createChannel()
        aggregateConfig = new AggregateConfig()
    }

    def "should get publish exchange"() {

        given:
        String routingKey = "routingKey"

        when:
        def exchange = aggregateConfig.getPublishExchange(routingKey)

        then:
        exchange == TopicData.SECURED_OUTGOING_EXCHANGE
    }

    def "should get queses to subscribe"() {

        given:
        def clientId = UUID.randomUUID()

        when:
        def quesesToSubscribe = aggregateConfig.getQueuesToSubscribe(clientId)

        then:
        quesesToSubscribe.size() == 2

        def queueForClient = quesesToSubscribe.get(0)
        queueForClient == "aggregate" + "-" + clientId

        def securedQueue = quesesToSubscribe.get(1)
        securedQueue == TopicData.SECURED_QUEUE
    }

    def "should declare queues"() {

        given:
        def clientId = UUID.randomUUID()
        String message = "message"

        when:
        aggregateConfig.declareQueuesConfig(channel, clientId)
        channel.basicPublish(TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.PRIVATE_ROUTING_KEY + clientId, null, message.getBytes())
        channel.basicPublish(TopicData.SECURED_INCOMING_EXCHANGE, "*.*", null, message.getBytes())

        then:
        def aggregateResponse = channel.basicGet("aggregate-" + clientId, false)
        aggregateResponse.getBody() == message.getBytes()
        aggregateResponse.getMessageCount() == 0

        def securedResponse = channel.basicGet(TopicData.SECURED_QUEUE, true)
        securedResponse.getBody() == message.getBytes()
        securedResponse.getMessageCount() == 0
    }
}