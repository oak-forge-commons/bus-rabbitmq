package oak.forge.commons.rabbitmq.topic

import com.rabbitmq.client.Channel
import oak.forge.commons.rabbitmq.topic.factory.FilteringConfig
import oak.forge.commons.rabbitmq.topic.factory.TopicData
import spock.lang.Shared

class FilteringConfigTest extends RabbitMqContainerConfig{

    @Shared
    FilteringConfig filteringConfig

    @Shared
    Channel channel

    void setupSpec() {

        container.start()
        channel = createChannel()
        filteringConfig = new FilteringConfig()
    }

    def "should get error publish exchange"(){

        given:
        String routingKey = "routingKey"

        when:
        def exchange = filteringConfig.getPublishExchange(routingKey)

        then:
        exchange == TopicData.SECURED_OUTGOING_EXCHANGE
    }

    def "should get default publish exchange "(){

        given:
        String routingKey = MessageRoutingType.COMMAND.getRouting()

        when:
        def exchange = filteringConfig.getPublishExchange(routingKey)

        then:
        exchange == TopicData.SECURED_INCOMING_EXCHANGE
    }

    def "should get queses to subscribe"() {

        given:
        def clientId = UUID.randomUUID()

        when:
        def quesesToSubscribe = filteringConfig.getQueuesToSubscribe(clientId)

        then:
        quesesToSubscribe.size() == 1
        def eventsQueue = quesesToSubscribe.get(0)
        eventsQueue == TopicData.UNSECURED_QUEUE
    }

    def "should declare queues"(){

        given:
        def clientId = UUID.randomUUID()
        String message = "message"

        when:
        filteringConfig.declareQueuesConfig(channel, clientId)
        channel.basicPublish(TopicData.UNSECURED_EXCHANGE, "*.*", null, message.getBytes())

        then:
        def messageFromQueue = channel.basicGet(TopicData.UNSECURED_QUEUE, true)
        messageFromQueue.getBody() == message.getBytes()
        messageFromQueue.getMessageCount() == 0
    }
}